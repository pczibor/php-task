<?php
    $script = <<<END
    let offset = 6;

    function loadRows() {
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            if (this.responseText == "done") {
                var btn = document.getElementById("btn_lmore");
                
                if (btn.style.display != "none")
                    btn.style.display = "none";
                return;
    
            }
            tbody = document.getElementById('tbl').getElementsByTagName('tbody')[0];
            tbody.innerHTML = tbody.innerHTML + this.responseText;
            offset = offset + 3;
        }
        xhttp.open("GET", "index.php?offset=" + offset, true);
        xhttp.send();
    }
\n
END;
    $header = <<<END
        <html>
            <head>
                <style>
                    table, th, td {
                        border: 1px solid;
                    }

                    img {
                        width: 100px;
                    }
                </style>
                <script>%s</script>
            </head>
            <body>

\n
END;

    $end_doc = "</body></html>";