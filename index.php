<?php
    include_once("template.php");

    class Product {
        public $name;
        public $description;
        public $url;
        public $image;
        public $price;

        function __construct($name, $description, $url, $image, $price) {
            $this->name = $name;
            $this->description = $description;
            $this->url = $url;
            $this->image = $image;
            $this->price = $price;
        }

        function print_as_tbrow() {
            $tbrow = "<tr>
                        <td>%s</td>
                        <td>%s</td>
                        <td><a href='%s' target='_blank'>link</a></td>
                        <td><img src='%s'/></td>
                        <td>%s</td>
                    </tr>";

            echo sprintf(
                $tbrow,
                $this->name,
                $this->description,
                $this->url,
                $this->image,
                $this->price
            );
        }

        function getPrice() {
            return $this->price;
        }
    }

    $products = array();
    function load_products($path, &$products) {
        $sxml = simplexml_load_file($path);
        foreach($sxml as $shopitem) {
            $product = new Product(
                $shopitem->PRODUCTNAME,
                $shopitem->DESCRIPTION,
                $shopitem->URL,
                $shopitem->IMGURL,
                $shopitem->PRICE
            );
            
            array_push($products, $product);
        }
    }

    load_products("products_1.xml", $products);
    load_products("products_2.xml", $products);
    load_products("products_3.xml", $products);

    usort($products, function ($p1, $p2) {
        if ((float)$p1->getPrice() === (float)$p2->getPrice()) {
            return 0;
        }
        return (float)$p1->getPrice() < (float)$p2->getPrice() ? -1 : 1;
    });

    if (isset($_GET['offset'])) {
        $offset = $_GET['offset'];
        $count = count($products);
        
        if (0 > $offset) $offset = 6;
        if ($count < $offset || $count < $offset + 3) {
            echo "done";
            return;
        }

        for ($i = $offset -3; $i < $offset; ++$i) {
            if (!array_key_exists($i, $products))
                return;
            
            $products[$i]->print_as_tbrow();
        }

        return;
    }

    echo sprintf($header, $script);

    echo "<table id=\"tbl\"><tbody id=\"tbl_body\">";
    for ($i=0; $i<3; ++$i){
        $products[$i]->print_as_tbrow();
    }
    echo "</tbody></table>";

    $button = "<button id='btn_lmore' onclick=\"loadRows()\">Load more</button>";
    echo $button;
    echo $end_doc;